package net.pl3x.bukkit.antiplayercramming.configuration;

import net.pl3x.bukkit.antiplayercramming.AntiPlayerCramming;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;

    public static void reload() {
        AntiPlayerCramming plugin = AntiPlayerCramming.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
    }
}
