package net.pl3x.bukkit.antiplayercramming;

import net.pl3x.bukkit.antiplayercramming.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
    private static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + AntiPlayerCramming.getPlugin().getName() + "&3]&r " + msg);
        if (!Config.COLOR_LOGS) {
            msg = ChatColor.stripColor(msg);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(msg);
    }

    public static void info(String msg) {
        Logger.log("&e[&fINFO&e]&r " + msg);
    }
}
