package net.pl3x.bukkit.antiplayercramming.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerListener implements Listener {
    @EventHandler
    public void onPlayerSuffocate(EntityDamageEvent event) {
        if (event.getCause() != EntityDamageEvent.DamageCause.CRAMMING) {
            return;
        }

        if (event.getEntity() instanceof Player) {
            event.setCancelled(true);
        }
    }
}
