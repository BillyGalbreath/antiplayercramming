package net.pl3x.bukkit.antiplayercramming;

import net.pl3x.bukkit.antiplayercramming.configuration.Config;
import net.pl3x.bukkit.antiplayercramming.listener.PlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

public class AntiPlayerCramming extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static AntiPlayerCramming getPlugin() {
        return AntiPlayerCramming.getPlugin(AntiPlayerCramming.class);
    }
}
